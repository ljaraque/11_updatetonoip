### file `updateToNoip.py`   

This python script reports the current IP public address of my system to Noip DNS.   

It must run as cron job in order to update periodically, with:   

(for example every 30 minutes)

*/30 * * * * root /usr/bin/python /path/to/updateNoIp.py

